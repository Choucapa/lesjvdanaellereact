import { schema } from 'normalizr';

// We use this Normalizr schemas to transform API responses from a nested form
// to a flat form where repos and users are placed in `entities`, and nested
// JSON objects are replaced with their IDs. This is very convenient for
// consumption by reducers, because we can easily build a normalized tree
// and keep it updated as we fetch more data.

// Read more about Normalizr: https://github.com/gaearon/normalizr

const membreSchema = new schema.Entity('membres', {}, { idAttribute: 'id' });
const commentaireSchema = new schema.Entity('commentaire', {}, { idAttribute: 'id' });
const jeuSchema = new schema.Entity('jeux', {}, { idAttribute: 'id' });

// Schemas for API responses.
export const Schemas = {
    MEMBRE: membreSchema,
    MEMBRES_ARRAY: new schema.Array(membreSchema),
    JEU: jeuSchema,
    JEUX_ARRAY: new schema.Array(jeuSchema),
    COMMENTAIRE: commentaireSchema,
    COMMENTAIRES_ARRAY: new schema.Array(commentaireSchema),
};

// Action key that carries API call info interpreted by this Redux middleware.
export const CALL_API = Symbol('Call API');

export default {
    entities: {
        jeux: {},
        membres: {},
        commentaires: {}
    },
    loaded: {
        appstorage: false,
        jeux: false,
        membres: false,
        commentaires: false
    },
    loading: {
        jeux: false,
        membres: false,
        commentaires: false
    }
};

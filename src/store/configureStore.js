import { configureStore } from '@reduxjs/toolkit';
import {
    persistStore,
    persistReducer,
    FLUSH,
    REHYDRATE,
    PAUSE,
    PERSIST,
    PURGE,
    REGISTER,
} from 'redux-persist';
import { createLogger } from 'redux-logger';

import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import rootReducer from './modules/reducer';
import api from './middleware/api';
const logger = createLogger();

const persistConfig = {
    key: 'ljva-webapp',
    storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export default () => {
    const store = configureStore({
        reducer: persistedReducer,
        // middleware: (getDefaultMiddleware) => {
        //   return getDefaultMiddleware().concat([thunk, api, logger]);
        // },
        middleware: (getDefaultMiddleware) => {
            return getDefaultMiddleware({
                serializableCheck: {
                    ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
                },
            }).concat([thunk, api, logger]);
        },
    }); // {} = initialState
    return { store, persistor: persistStore(store) };
};

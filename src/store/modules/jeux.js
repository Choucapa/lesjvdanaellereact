import {flow, set} from 'lodash/fp';
import {omit} from 'lodash';
import {CALL_API, Schemas} from '../schemas'

const FETCH_ALL_JEUX_REQUEST = 'ljva/FETCH_ALL_JEUX_REQUEST';
const FETCH_ALL_JEUX_SUCCESS = 'ljva/FETCH_ALL_JEUX_SUCCESS';
const FETCH_ALL_JEUX_FAILURE = 'ljva/FETCH_ALL_JEUX_FAILURE';

const FETCH_JEU_REQUEST = 'ljva/FETCH_ALL_JEUX_REQUEST';
const FETCH_JEU_SUCCESS = 'ljva/FETCH_ALL_JEUX_SUCCESS';
const FETCH_JEU_FAILURE = 'ljva/FETCH_ALL_JEUX_FAILURE';

const CREATE_JEUX_REQUEST = 'ljva/CREATE_JEUX_REQUEST';
const CREATE_JEUX_SUCCESS = 'ljva/CREATE_JEUX_SUCCESS';
const CREATE_JEUX_FAILURE = 'ljva/CREATE_JEUX_FAILURE';

const DELETE_JEU_REQUEST = 'ljva/DELETE_JEUX_REQUEST';
const DELETE_JEU_SUCCESS = 'ljva/DELETE_JEUX_SUCCESS';
const DELETE_JEU_FAILURE = 'ljva/DELETE_JEUX_FAILURE';

const UPDATE_JEU_REQUEST = 'ljva/UPDATE_JEUX_REQUEST';
const UPDATE_JEU_SUCCESS = 'ljva/UPDATE_JEUX_SUCCESS';
const UPDATE_JEU_FAILURE = 'ljva/UPDATE_JEUX_FAILURE';

export const jeuxActionsHandlers = {
    [FETCH_ALL_JEUX_REQUEST]: (state) =>
        flow(set('loaded.jeux', false), set('loading.jeux', true))(state),
    [FETCH_ALL_JEUX_SUCCESS]: (state, action) => {
        return flow(
            set('entities.jeux', action.response.entities.jeux || {}),
            set('loaded.jeux', true),
            set('loading.jeux', false)
        )(state);
    },
    [FETCH_ALL_JEUX_FAILURE]: (state) =>
        flow(set('loaded.jeux', false), set('loading.jeux', false))(state),
    [FETCH_JEU_REQUEST]: (state) =>
        flow(set('loaded.jeux', false), set('loading.jeux', true))(state),
    [FETCH_JEU_SUCCESS]: (state, action) => {
        return flow(
            set('entities.jeux', action.response.entities.jeux || {}),
            set('loaded.jeux', true),
            set('loading.jeux', false)
        )(state);
    },
    [FETCH_JEU_FAILURE]: (state) =>
        flow(set('loaded.jeux', false), set('loading.jeux', false))(state),
    [CREATE_JEUX_SUCCESS]: (state, action) => {
        return flow(
            set('entities.jeux', {
                ...state.entities.jeux,
                ...action.response.entities.jeux
            }),
            set('loaded.jeux', true),
            set('loading.jeux', false)
        )(state);
    },

    [DELETE_JEU_SUCCESS]: (state, action) =>
        flow(
            set('entities.jeux', omit(state.entities.jeux, [action.jeuId]))
        )(state)
};

// CREATE

export function createJeux(data) {
    console.log("GOOD JOB")
    return {
        [CALL_API]: {
            types: [
                CREATE_JEUX_REQUEST,
                CREATE_JEUX_SUCCESS,
                CREATE_JEUX_FAILURE
            ],
            method: 'post',
            endpoint: '/jeux/create',
            schema: Schemas.JEU,
            body: data
        }
    };
}

// READ

export function getJeux() {
    return {
        [CALL_API]: {
            types: [
                FETCH_ALL_JEUX_REQUEST,
                FETCH_ALL_JEUX_SUCCESS,
                FETCH_ALL_JEUX_FAILURE
            ],
            method: 'GET',
            endpoint: '/jeux',
            schema: Schemas.JEUX_ARRAY,
            successMessage: 'Jeux chargés avec succès'
        }
    };
}

export function getJeuById(jeuId) {
    console.log("TEST")
    return {

        [CALL_API]: {
            types: [
                FETCH_JEU_REQUEST,
                FETCH_JEU_SUCCESS,
                FETCH_JEU_FAILURE
            ],
            method: 'GET',
            endpoint: `/jeux/${jeuId}`,
            schema: Schemas.JEUX_ARRAY,
            successMessage: 'Jeu chargé avec succès'
        }
    }
}


// UPDATE

export function updateJeu(jeu, data) {
    return {
        jeu,
        [CALL_API]: {
            types: [
                UPDATE_JEU_REQUEST,
                UPDATE_JEU_SUCCESS,
                UPDATE_JEU_FAILURE
            ],
            method: 'PUT',
            body: data,
            endpoint: `/jeux/${jeu.id}`,
            successMessage: 'Le jeu a bien été modifié'
        }
    };
}

// DELETE

export function deleteJeu(jeuId) {
    return {
        jeuId,
        [CALL_API]: {
            types: [
                DELETE_JEU_REQUEST,
                DELETE_JEU_SUCCESS,
                DELETE_JEU_FAILURE
            ],
            method: 'DELETE',
            endpoint: `/jeux/${jeuId}`,
            successMessage: 'Le jeu a bien été supprimé'
        }
    };
}

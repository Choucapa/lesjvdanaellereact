import { flow, set } from 'lodash/fp';
import { omit } from 'lodash';
import { CALL_API, Schemas } from '../schemas';

const FETCH_ALL_COMMENTAIRES_REQUEST = 'ljva/FETCH_ALL_COMMENTAIRES_REQUEST';
const FETCH_ALL_COMMENTAIRES_SUCCESS = 'ljva/FETCH_ALL_COMMENTAIRES_SUCCESS';
const FETCH_ALL_COMMENTAIRES_FAILURE = 'ljva/FETCH_ALL_COMMENTAIRES_FAILURE';

const FETCH_COMMENTAIRE_REQUEST = 'ljva/FETCH_ALL_COMMENTAIRES_REQUEST';
const FETCH_COMMENTAIRE_SUCCESS = 'ljva/FETCH_ALL_COMMENTAIRES_SUCCESS';
const FETCH_COMMENTAIRE_FAILURE = 'ljva/FETCH_ALL_COMMENTAIRES_FAILURE';

const CREATE_COMMENTAIRE_REQUEST = 'ljva/CREATE_COMMENTAIRES_REQUEST';
const CREATE_COMMENTAIRE_SUCCESS = 'ljva/CREATE_COMMENTAIRES_SUCCESS';
const CREATE_COMMENTAIRE_FAILURE = 'ljva/CREATE_COMMENTAIRES_FAILURE';

const DELETE_COMMENTAIRE_REQUEST = 'ljva/DELETE_COMMENTAIRES_REQUEST';
const DELETE_COMMENTAIRE_SUCCESS = 'ljva/DELETE_COMMENTAIRES_SUCCESS';
const DELETE_COMMENTAIRE_FAILURE = 'ljva/DELETE_COMMENTAIRES_FAILURE';

const UPDATE_COMMENTAIRE_REQUEST = 'ljva/UPDATE_COMMENTAIRES_REQUEST';
const UPDATE_COMMENTAIRE_SUCCESS = 'ljva/UPDATE_COMMENTAIRES_SUCCESS';
const UPDATE_COMMENTAIRE_FAILURE = 'ljva/UPDATE_COMMENTAIRES_FAILURE';

export const commentairesActionsHandlers = {
    [FETCH_ALL_COMMENTAIRES_REQUEST]: (state) =>
        flow(set('loaded.commentaires', false), set('loading.commentaires', true))(state),
    [FETCH_ALL_COMMENTAIRES_SUCCESS]: (state, action) => {
        return flow(
            set('entities.commentaires', action.response.entities.commentaires || {}),
            set('loaded.commentaires', true),
            set('loading.commentaires', false)
        )(state);
    },
    [FETCH_ALL_COMMENTAIRES_FAILURE]: (state) =>
        flow(set('loaded.commentaires', false), set('loading.commentaires', false))(state),
    [FETCH_COMMENTAIRE_REQUEST]: (state) =>
        flow(set('loaded.commentaires', false), set('loading.commentaires', true))(state),
    [FETCH_COMMENTAIRE_SUCCESS]: (state, action) => {
        return flow(
            set('entities.commentaires', action.response.entities.commentaires || {}),
            set('loaded.commentaires', true),
            set('loading.commentaires', false)
        )(state);
    },
    [FETCH_COMMENTAIRE_FAILURE]: (state) =>
        flow(set('loaded.commentaires', false), set('loading.commentaires', false))(state),

    [CREATE_COMMENTAIRE_SUCCESS]: (state, action) => {
        return flow(
            set('entities.commentaires', {
                ...state.entities.commentaires,
                ...action.response.entities.commentaires
            }),
            set('loaded.commentaires', true),
            set('loading.commentaires', false)
        )(state);
    },

    [UPDATE_COMMENTAIRE_SUCCESS]: (state, action) => {
        return flow(
            set(`entities.commentaires.${action.commentaires.id}`, {
                ...action.commentaires,
                ...action.response
            }),
            set('loaded.commentaires', true),
            set('loading.commentaires', false)
        )(state);
    },

    [DELETE_COMMENTAIRE_SUCCESS]: (state, action) =>
        flow(
            set('entities.commentaires', omit(state.entities.commentaires, [action.id]))
        )(state)
};

// CREATE

export function createCommentaire(data) {
    return {
        [CALL_API]: {
            types: [
                CREATE_COMMENTAIRE_REQUEST,
                CREATE_COMMENTAIRE_SUCCESS,
                CREATE_COMMENTAIRE_FAILURE
            ],
            method: 'POST',
            endpoint: '/commentaires/create',
            schema: Schemas.COMMENTAIRES,
            body: data
        }
    };
}

// READ

export function loadCommentaires() {
    return {
        [CALL_API]: {
            types: [
                FETCH_ALL_COMMENTAIRES_REQUEST,
                FETCH_ALL_COMMENTAIRES_SUCCESS,
                FETCH_ALL_COMMENTAIRES_FAILURE
            ],
            method: 'GET',
            endpoint: '/commentaires',
            schema: Schemas.COMMENTAIRES_ARRAY,
            successMessage: 'commentaires chargés avec succès'
        }
    };
}

export function loadCommentaire(commentaireId) {
    return {
        [CALL_API]: {
            types: [
                FETCH_COMMENTAIRE_REQUEST,
                FETCH_COMMENTAIRE_SUCCESS,
                FETCH_COMMENTAIRE_FAILURE
            ],
            method: 'GET',
            endpoint: `/commentaires/${commentaireId}`,
            schema: Schemas.COMMENTAIRE,
            successMessage: 'commentaire chargé avec succès'
        }
    };
}

// UPDATE

export function updateCommentaire(commentaire, data) {
    return {
        commentaire,
        [CALL_API]: {
            types: [
                UPDATE_COMMENTAIRE_REQUEST,
                UPDATE_COMMENTAIRE_SUCCESS,
                UPDATE_COMMENTAIRE_FAILURE
            ],
            method: 'PUT',
            body: data,
            endpoint: `/commentaires/${commentaire.id}`,
            successMessage: 'Le commentaire a bien été modifié'
        }
    };
}

// DELETE

export function deleteCommentaire(CommentaireId) {
    return {
        CommentaireId,
        [CALL_API]: {
            types: [
                DELETE_COMMENTAIRE_REQUEST,
                DELETE_COMMENTAIRE_SUCCESS,
                DELETE_COMMENTAIRE_FAILURE
            ],
            method: 'DELETE',
            endpoint: `/commentaires/${CommentaireId}`,
            successMessage: 'Le commentaire a bien été supprimé'
        }
    };
}

import { CALL_API } from '../schemas';

const UPLOADS_REQUEST = 'ljvda/uploads/UPLOADS_REQUEST';
const UPLOADS_SUCCESS = 'ljvda/uploads/UPLOADS_SUCCESS';
const UPLOADS_FAILURE = 'ljvda/uploads/UPLOADS_FAILURE';
const DELETE_REQUEST = 'ljvda/uploads/DELETE_REQUEST';
const DELETE_SUCCESS = 'ljvda/uploads/DELETE_SUCCESS';
const DELETE_FAILURE = 'ljvda/uploads/DELETE_FAILURE';

export const uploadsActionsHandlers = {};

export function uploadFiles(files) {
    return {
        [CALL_API]: {
            types: [UPLOADS_REQUEST, UPLOADS_SUCCESS, UPLOADS_FAILURE],
            method: 'POST',
            endpoint: `/uploads`,
            body: files,
            upload: true,
        },
    };
}

export function removeFile(filesToRemove) {
    return {
        [CALL_API]: {
            types: [DELETE_REQUEST, DELETE_SUCCESS, DELETE_FAILURE],
            method: 'DELETE',
            endpoint: `/uploads`,
            body: {
                filesToRemove,
            },
            upload: true,
        },
    };
}

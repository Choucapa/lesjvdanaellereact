import {CALL_API, Schemas} from '../schemas';

const LOAD_REQUEST = 'ljvda/auth/LOAD_REQUEST';
const LOAD_SUCCESS = 'ljvda/auth/LOAD_SUCCESS';
const LOAD_FAIL = 'ljvda/auth/LOAD_FAIL';

const LOGIN_REQUEST = 'ljvda/auth/LOGIN_REQUEST';
const LOGIN_SUCCESS = 'ljvda/auth/LOGIN_SUCCESS';
const LOGIN_FAIL = 'ljvda/auth/LOGIN_FAIL';

const REGISTER_REQUEST = 'ljvda/auth/REGISTER_REQUEST';
const REGISTER_SUCCESS = 'ljvda/auth/REGISTER_SUCCESS';
const REGISTER_FAIL = 'ljvda/auth/REGISTER_FAIL';

const LOGOUT_REQUEST = 'ljvda/auth/LOGOUT_REQUEST';
const LOGOUT_SUCCESS = 'ljvda/auth/LOGOUT_SUCCESS';
const LOGOUT_FAIL = 'ljvda/auth/LOGOUT_FAIL';

const CREATE_PASSWORD_REQUEST = 'ljvda/auth/CREATE_PASSWORD_REQUEST';
const CREATE_PASSWORD_SUCCESS = 'ljvda/auth/CREATE_PASSWORD_SUCCESS';
const CREATE_PASSWORD_FAILURE = 'ljvda/auth/CREATE_PASSWORD_FAILURE';

const RECOVER_PASSWORD_REQUEST = 'ljvda/auth/RECOVER_PASSWORD_REQUEST';
const RECOVER_PASSWORD_SUCCESS = 'ljvda/auth/RECOVER_PASSWORD_SUCCESS';
const RECOVER_PASSWORD_FAILURE = 'ljvda/auth/RECOVER_PASSWORD_FAILURE';

const UPDATE_USER_REQUEST = 'ljvda/UPDATE_USER_REQUEST';
const UPDATE_USER_SUCCESS = 'ljvda/UPDATE_USER_SUCCESS';
const UPDATE_USER_FAILURE = 'ljvda/UPDATE_USER_FAILURE';

const CHECK_EMAIL_REQUEST = 'ljvda/auth/CHECK_EMAIL_REQUEST';
const CHECK_EMAIL_SUCCESS = 'ljvda/auth/CHECK_EMAIL_SUCCESS';
const CHECK_EMAIL_FAILURE = 'ljvda/auth/CHECK_EMAIL_FAILURE';

const GET_ROLE_REQUEST = 'ljvda/auth/GET_ROLE_REQUEST';
const GET_ROLE_SUCCESS = 'ljvda/auth/GET_ROLE_SUCCESS';
const GET_ROLE_FAILURE = 'ljvda/auth/GET_ROLE_FAILURE';

const initialState = {
    loaded: false,
    isConnected: false
};

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case LOAD_REQUEST:
            return {
                ...state,
                loading: true
            };
        case LOAD_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                user: action.response
            };
        case LOAD_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.error
            };
        case LOGIN_REQUEST:
            return {
                ...state,
                loggingIn: true
            };
        case LOGIN_SUCCESS: {
            return {
                ...state,
                loggingIn: false,
                isConnected: true,
                user: action.response
            };
        }

        case LOGIN_FAIL:
            return {
                ...state,
                loggingIn: false,
                user: null,
                loginError: action.error
            };

        case LOGOUT_REQUEST:
            return {
                ...state,
                loggingOut: true
            };
        case LOGOUT_SUCCESS:
            return {
                ...state,
                loggingOut: false,
                user: null
            };
        case LOGOUT_FAIL:
            return {
                ...state,
                loggingOut: false,
                logoutError: action.error
            };
        case UPDATE_USER_SUCCESS:
            return {
                ...state,
                user: {...state.user, ...action.response},
                loggingOut: false,
                logoutError: action.error
            };

        default:
            return state;
    }
}

export function isLoaded(globalState) {
    return globalState.auth && globalState.auth.loaded;
}

export function loadUser(userId) {
    return {
        userId,
        [CALL_API]: {
            types: [LOAD_REQUEST, LOAD_SUCCESS, LOAD_FAIL],
            method: 'GET',
            endpoint: `/membres/${parseInt(userId, 10)}`,
            schema: Schemas.UTILISATEUR_ARRAY
        }
    };
}


export function login(email, password) {
    return {
        [CALL_API]: {
            types: [LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAIL],
            method: 'POST',
            endpoint: '/auth/login',
            schema: Schemas.LOGIN,
            body: {email, password},
            applyNormalize: false
        }
    };
}

export function register(data) {
    return {
        [CALL_API]: {
            types: [REGISTER_REQUEST, REGISTER_SUCCESS, REGISTER_FAIL],
            method: 'POST',
            endpoint: '/users/create',
            body: data
        }
    };
}

export function createPassword(data) {
    return {
        [CALL_API]: {
            types: [CREATE_PASSWORD_REQUEST, CREATE_PASSWORD_SUCCESS, CREATE_PASSWORD_FAILURE],
            method: 'PUT',
            endpoint: '/auth/reset-password',
            body: data
        }
    };
}

export function logout() {
    return {
        type: LOGOUT_SUCCESS
    };
}

export function recoverPassword(email) {
    return {
        [CALL_API]: {
            types: [RECOVER_PASSWORD_REQUEST, RECOVER_PASSWORD_SUCCESS, RECOVER_PASSWORD_FAILURE],
            method: 'PUT',
            endpoint: '/auth/password',
            body: {
                email
            }
        }
    };
}

export function updateUser(user, data) {
    return {
        user,
        [CALL_API]: {
            types: [UPDATE_USER_REQUEST, UPDATE_USER_SUCCESS, UPDATE_USER_FAILURE],
            method: 'PUT',
            schema: Schemas.USER_ARRAY,
            body: data,
            endpoint: `/users/${user.utilisateur_id}`,
            successMessage: `L'User a bien été modifié !`
        }
    };
}

export function checkEmail(email) {
    return {
        [CALL_API]: {
            types: [CHECK_EMAIL_REQUEST, CHECK_EMAIL_SUCCESS, CHECK_EMAIL_FAILURE],
            method: 'POST',
            endpoint: '/auth/checkEmail',
            body: {
                email
            }
        }
    };
}

export function getRole(token) {
    return {
        [CALL_API]: {
            types: [GET_ROLE_REQUEST, GET_ROLE_SUCCESS, GET_ROLE_FAILURE],
            method: 'GET',
            endpoint: `/auth/getRole/${token}`,
            successMessage: 'Rôle récupéré avec succès !'
        }
    };
}

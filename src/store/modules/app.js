import initialState from '../initialState';
import '../../index';

import { jeuxActionsHandlers } from './jeux';
import { membresActionsHandlers } from './membres';
import { commentairesActionsHandlers } from './commentaires';
import { uploadsActionsHandlers } from './uploads';

const actionsHandlers = {
    ...jeuxActionsHandlers,
    ...commentairesActionsHandlers,
    ...membresActionsHandlers,
    ...uploadsActionsHandlers,
};

export default function reducer(state = initialState, action) {
    const reduceFn = actionsHandlers[action.type];
    return reduceFn ? reduceFn(state, action) : state;
}

import { flow, set } from 'lodash/fp';
import { omit } from 'lodash';
import { CALL_API, Schemas } from '../schemas';

const FETCH_ALL_MEMBRES_REQUEST = 'ljva/FETCH_ALL_MEMBRES_REQUEST';
const FETCH_ALL_MEMBRES_SUCCESS = 'ljva/FETCH_ALL_MEMBRES_SUCCESS';
const FETCH_ALL_MEMBRES_FAILURE = 'ljva/FETCH_ALL_MEMBRES_FAILURE';

const FETCH_MEMBRE_REQUEST = 'ljva/FETCH_MEMBRE_REQUEST';
const FETCH_MEMBRE_SUCCESS = 'ljva/FETCH_MEMBRE_SUCCESS';
const FETCH_MEMBRE_FAILURE = 'ljva/FETCH_MEMBRE_FAILURE';

const CREATE_MEMBRE_REQUEST = 'ljva/CREATE_MEMBRES_REQUEST';
const CREATE_MEMBRE_SUCCESS = 'ljva/CREATE_MEMBRES_SUCCESS';
const CREATE_MEMBRE_FAILURE = 'ljva/CREATE_MEMBRES_FAILURE';

const DELETE_MEMBRE_REQUEST = 'ljva/DELETE_MEMBRES_REQUEST';
const DELETE_MEMBRE_SUCCESS = 'ljva/DELETE_MEMBRES_SUCCESS';
const DELETE_MEMBRE_FAILURE = 'ljva/DELETE_MEMBRES_FAILURE';

const UPDATE_MEMBRE_REQUEST = 'ljva/UPDATE_MEMBRES_REQUEST';
const UPDATE_MEMBRE_SUCCESS = 'ljva/UPDATE_MEMBRES_SUCCESS';
const UPDATE_MEMBRE_FAILURE = 'ljva/UPDATE_MEMBRES_FAILURE';

export const membresActionsHandlers = {
    [FETCH_ALL_MEMBRES_REQUEST]: (state) =>
        flow(set('loaded.membres', false), set('loading.membres', true))(state),
    [FETCH_ALL_MEMBRES_SUCCESS]: (state, action) => {
        return flow(
            set('entities.membres', action.response.entities.membres || {}),
            set('loaded.membres', true),
            set('loading.membres', false)
        )(state);
    },
    [FETCH_ALL_MEMBRES_FAILURE]: (state) =>
        flow(set('loaded.membres', false), set('loading.membres', false))(state),


    [CREATE_MEMBRE_SUCCESS]: (state, action) => {
        return flow(
            set('entities.membres', {
                ...state.entities.membres,
                ...action.response.entities.membres
            }),
            set('loaded.membres', true),
            set('loading.membres', false)
        )(state);
    },

    [UPDATE_MEMBRE_SUCCESS]: (state, action) => {
        return flow(
            set(`entities.membres.${action.membre.id}`, {
                ...action.membres,
                ...action.response
            }),
            set('loaded.membres', true),
            set('loading.membres', false)
        )(state);
    },

    [DELETE_MEMBRE_SUCCESS]: (state, action) =>
        flow(
            set('entities.membres', omit(state.entities.membres, [action.membreId]))
        )(state)
};

// CREATE

export function createMembres(data) {
    return {
        [CALL_API]: {
            types: [
                CREATE_MEMBRE_REQUEST,
                CREATE_MEMBRE_SUCCESS,
                CREATE_MEMBRE_FAILURE
            ],
            method: 'POST',
            endpoint: '/membres/create',
            schema: Schemas.MEMBRE,
            body: data
        }
    };
}

// READ

export function loadMembres() {
    return {
        [CALL_API]: {
            types: [
                FETCH_ALL_MEMBRES_REQUEST,
                FETCH_ALL_MEMBRES_SUCCESS,
                FETCH_ALL_MEMBRES_FAILURE
            ],
            method: 'GET',
            endpoint: '/membres',
            schema: Schemas.MEMBRES_ARRAY,
            successMessage: 'Membres chargés avec succès'
        }
    };
}

export function loadMembre(membreId) {
    return {
        [CALL_API]: {
            types: [
                FETCH_MEMBRE_REQUEST,
                FETCH_MEMBRE_SUCCESS,
                FETCH_MEMBRE_FAILURE
            ],
            method: 'GET',
            endpoint: `/membres/${membreId}`,
            schema: Schemas.MEMBRE,
            successMessage: 'Membre chargé avec succès'
        }
    };
}

// UPDATE

export function updateMEMBRE(membre, data) {
    return {
        membre,
        [CALL_API]: {
            types: [
                UPDATE_MEMBRE_REQUEST,
                UPDATE_MEMBRE_SUCCESS,
                UPDATE_MEMBRE_FAILURE
            ],
            method: 'PUT',
            body: data,
            endpoint: `/membres/${membre.id}`,
            successMessage: 'Le membre a bien été modifié'
        }
    };
}

// DELETE

export function deleteMembre(membreId) {
    return {
        membreId,
        [CALL_API]: {
            types: [
                DELETE_MEMBRE_REQUEST,
                DELETE_MEMBRE_SUCCESS,
                DELETE_MEMBRE_FAILURE
            ],
            method: 'DELETE',
            endpoint: `/membres/${membreId}`,
            successMessage: 'Le jeu a bien été supprimé'
        }
    };
}

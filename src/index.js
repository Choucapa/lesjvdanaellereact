import React from 'react';
import './index.css';
import Home from './pages/Home';
import 'bootstrap/dist/css/bootstrap.min.css';

import {Provider} from 'react-redux';
import configureStore from './store/configureStore';
import {PersistGate} from 'redux-persist/integration/react';

import {createRoot} from 'react-dom/client';
import {BrowserRouter} from "react-router-dom";

const { persistor, store } = configureStore();
const container = document.getElementById('root');
const root = createRoot(container);

root.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <BrowserRouter>
                <Home tab="home"/>
            </BrowserRouter>);
        </PersistGate>
    </Provider>
)


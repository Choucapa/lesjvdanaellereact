import React, {useState} from "react";
import "./add-commentaire.css"
import axios from "axios";
import {useNavigate} from "react-router-dom";


const AddCommentaire = () => {

    const [com, setCom] = useState('')
    const url_decoupe = document.location.href.split("/");
    const id_jeu = url_decoupe[url_decoupe.length - 1].slice();
    const navigate = useNavigate();

    const handleSubmit = () => {
        axios.post('http://localhost:3000/commentaires', {
            membre: 1, com: com, jeu: id_jeu
        }).then();
        navigate(`/desc/${id_jeu}`);
    }
    const handleChange = (event) => {
        setCom(event.target.value);
    }


    return (
        <div className="container-fluid ">
            <div className="bloc2  col-8 offset-2">
                <form onSubmit={handleSubmit}>
                    <input className="form-control" id="commentaire" rows="3"
                           placeholder="Ecrit ton commentaire ici !" required
                           onChange={handleChange}
                    ></input>
                    <br/>
                    <button type="submit" className="btn btn-primary">
                        Ajouter un commentaire
                    </button>
                </form>
            </div>
        </div>
    )

}

export default AddCommentaire
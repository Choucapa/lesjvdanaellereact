import React, {Fragment, useEffect} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import axios from "axios";
import "./bloc-commentaire.css"

const BlocCommentaire = () => {

    const url_decoupe = document.location.href.split("/");
    const id_jeu = url_decoupe[url_decoupe.length - 1].slice();

    const [commentaires, setCommentaires] = React.useState(null);
    useEffect(() => {
        axios.get("http://localhost:3000/commentaires").then((response) => {
            setCommentaires(response.data);
        })
    }, []);

    let listeCommentaires = {};


    if (commentaires && commentaires.length) {
        listeCommentaires = commentaires.map(
            (commentaire) => (
                commentaire.jeu === id_jeu &&
                <div key={commentaire.id} className="card-com col-6 offset-3">
                    <div className="card-com-title">
                        <span>
                           {"Publié par Moi" + "                 "} <FontAwesomeIcon icon="trash"/>
                        </span>
                    </div>
                    <div className="card-com-text">
                        <span>{commentaire.com}</span>
                    </div>
                </div>
            ))
    } else {
        <div><p>Pas encore de commentaire</p></div>
    }


    return (
        <Fragment>{commentaires && commentaires.length && listeCommentaires}</Fragment>
    )

}
export default BlocCommentaire;
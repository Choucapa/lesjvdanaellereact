import '../App.css'


import AddJeu from "./Jeu/AddJeu/add-jeu";
import {Route, Routes} from "react-router-dom";
import React, {Fragment} from 'react';
import PageNotFound from "./notFound";
import Footer from "../core/components/footer/footer";
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../core/components/header/header";
import ListJeu from "./Jeu/ListJeu/list-jeu";
import {library} from '@fortawesome/fontawesome-svg-core'
import {fas} from '@fortawesome/free-solid-svg-icons'
import {faFontAwesome} from '@fortawesome/free-brands-svg-icons'
import Login from "./Login/login";
import AddMembre from "./Membre/AddMembre/add-membre";
import DescribeJeu from "./Jeu/DescribeJeu/describe-jeu";
import EditJeu from "./Jeu/EditJeu/edit-jeu";


library.add(fas, faFontAwesome)


class Home extends React.Component {
    render() {
        return (

            <div className="pagePrincipale">
                <div className="row">
                    <Header/>
                </div>
                <div className="container-fluid body row">
                    <div className="content col">
                        <Routes>
                            <Route exact path="/" element={<Accueil/>}/>
                            <Route exact path="/addjeu" element={<AddJeu/>}/>
                            <Route exact path="/addmembre" element={<AddMembre/>}/>
                            <Route exact path="/listjeu" element={<ListJeu/>}/>
                            <Route exact path="/login" element={<Login/>}/>
                            <Route path="/edit/:id" element={<EditJeu/>}/>
                            <Route path="/desc/:id" element={<DescribeJeu/>}/>*/
                            <Route path="*" element={<PageNotFound/>}/>
                        </Routes>
                    </div>
                </div>
                <div className="row">
                    <Footer/>
                </div>
            </div>

        );
    }
}

function Accueil() {
    return (
        <Fragment>
            <h3 className="text-center">
                Bienvenue sur les jeux-vidéo d'Anaëlle
            </h3>
            <div className="container-fluid">
                <div className="text offset-2 col-8">
                    <p>
                        Bienvenue sur les cette application. Ici vous trouverez mon bac à sable ou je m’exerce en réact
                        sur
                        différentes compétences que j’acquiers en stage et en formation pour devenir développeuse web.
                        Lorem ipsum dolor sit amet. Est maxime nobis est magni illo eos quia galisum et sequi maiores ut
                        accusamus consequatur quo corrupti velit aut nihil nemo. Sit voluptas accusantium et pariatur
                        provident ut voluptas maiores in nihil sunt qui nisi culpa sit nemo vero.
                    </p>
                    <p>
                        Eos internos recusandae eos molestias et repudiandae natus ut quisquam doloremque ut quia sequi
                        sed
                        voluptatem repellat ut cumque laudantium. Eum nihil rerum ea tenetur quod in debitis sunt id
                        nobis
                        modi ea tempora provident ut quia rerum qui officiis ratione. Aut ullam unde et omnis similique
                        At
                        eligendi harum.
                    </p>
                    <p>
                        Id exercitationem quia qui quia consequatur et provident rerum qui vero laborum hic officia
                        facilis.
                        Est quia earum sed commodi aspernatur est eveniet omnis aut labore eius qui sequi commodi. Ea
                        atque
                        commodi earum odit et laborum porro ut quisquam minima. Est repellat quidem sed quam velit ex
                        quia
                        quia et sequi nesciunt.
                    </p>
                </div>
            </div>
        </Fragment>
    )
}

export default Home;

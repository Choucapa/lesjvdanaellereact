import React from "react";
import {Link} from "react-router-dom";
import {Image} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

class ListMembre extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            listeMembre: null
        };
    }

    componentDidMount() {
        fetch("http://localhost:3000/membre")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const {error, isLoaded} = this.state;
        if (error) {
            return <div>Erreur : {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Chargement…</div>;
        } else {
            this.listeMembre = this.state.items.map(
                (membre) => (
                    <tr key={membre.id}>
                        <td>
                            {membre.pseudo}
                        </td>
                        <td>
                           <FontAwesomeIcon icon="trash"/>
                        </td>
                    </tr>
                )
            )
        }
        return (
            <div className="container">
                <table className="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Nom du membre</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.listeMembre}
                    </tbody>
                </table>
            </div>
        )
    }


}
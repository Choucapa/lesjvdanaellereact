import * as Yup from "yup";


export const validationSchema = Yup.object().shape({
    pseudo: Yup.string()
        .required('Le pseudo est requis.')
        .min(2,'Votre pseudo doit contenir au moins 2 caractères.')
        .max(50, 'Votre pseudo doit contenir moins de 50 caractères.'),
    password: Yup.string()
        .min(6, 'Le mot de passe doit contenir au moins 6 caractères.')
        .max(50, 'Le mot de passe doit contenir moins de 50 caractères.')
        .required('Le mot de passe est requis.'),
    confirmPassword: Yup.string()
        .oneOf([Yup.ref('password'), null], 'Les mots de passe doivent correspondre.')
        .required('La confirmation du mot de passe est requise.')
});


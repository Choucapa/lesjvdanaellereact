import React from "react";
import {validationSchema} from "./add-membre-validators";
import {ErrorMessage, Field, Form, Formik, useFormik} from "formik";

const initialValues = {
    pseudo: '',
    password: '',
    confirmPassword: '',
};

class AddMembre extends React.Component {

    render() {
        function handleSubmit(values) {
            console.log("Pourrais tu arreter d'appuyer sur ce bouton !?")
        }

        return (
            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={(values) => handleSubmit(values)}
            >
                {({resetForm}) => (
                    <Form>
                        <fieldset class="fieldset  m-5">

                            < div class="form-control m-3">
                                < label> Pseudo :</label>
                                <Field
                                    id="pseudo" name="pseudo"
                                    type="text" min="2"
                                    className="form-control"
                                />
                                <ErrorMessage name="pseudo"
                                              className="text-danger"
                                              component="small"/>
                            </div>

                            <div class="form-control m-3">
                                <label>Mot de passe : </label>
                                <Field type="password" min="2"
                                       id="password" name="password"
                                       className="form-control"/>
                                <ErrorMessage name="password"
                                              className="text-danger"
                                              component="small"/>
                            </div>

                            <div class="form-control m-3">
                                <label> Validation de mot de passe : </label>
                                <Field type="confirmPassword" min="2"
                                       id="confirmPassword" name="confirmPassword"
                                       className="form-control"/>
                                <ErrorMessage name="confirmPassword"
                                              className="text-danger"
                                              component="small"/>
                            </div>

                        </fieldset>

                        <button type="submit" className="btn btn-primary m-3 ">
                            Créer
                        < /button>
                        <button
                            type="button"
                            onClick={resetForm}
                            className="btn btn-danger"
                        >
                            Annuler
                        </button>

                    </Form>)}
            </Formik>
        )
    }
}

export default AddMembre;
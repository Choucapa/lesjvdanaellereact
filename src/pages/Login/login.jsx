import "./login.css"
import React, {Fragment, useState} from "react";
import {connect} from "react-redux";
import {NavLink, useNavigate} from "react-router-dom";
import {Field, Form} from "react-final-form";
import {login} from "../../store/modules/auth";
import {setConfig} from "../../store/modules/globals";
import PropTypes from 'prop-types';


const Login = ({login, setConfig}) => {


    const navigate = useNavigate();
    const [displayPassword, setDisplayPassword] = useState(false);
    const [error, setError] = useState(null);
    const required = (value) => (value ? undefined : 'Champ requis !');

    const handleSubmit = (values) => {
        return login(values.email, values.password)
            .then((res) => {
                const user = res.response;
                return navigate(`/`, {replace: true})
            })
            .then(() => setConfig('isLoading', false))
            .catch((err) => setError(err));
    };

    return (
        <Fragment>
            <main>
                <div className="bloc">
                    <div className="container">
                        <div className="grid">
                            <div className="col-8_sm-12">
                                <h2>Connexion</h2>
                                <div className="box">

                                    <div className="box-content">
                                        <Form
                                            onSubmit={handleSubmit}
                                            render={({handleSubmit}) => (<form onSubmit={handleSubmit}>
                                                <div className="form-body">
                                                    <Field
                                                        name="email"
                                                        component="input"
                                                        className="field"
                                                        validate={required}
                                                    >
                                                        {({input, meta}) => (<div className="field">
                                                            {meta.error && meta.touched && (<div
                                                                className="field-error">{meta.error}</div>)}
                                                            <input {...input} type="email"/>
                                                            <label>Adresse e-mail *</label>
                                                        </div>)}
                                                    </Field>
                                                    <Field
                                                        name="password"
                                                        component="input"
                                                        className="field"
                                                        validate={required}
                                                    >
                                                        {({input, meta}) => (<div className="field">
                                                            {meta.error && meta.touched && (<div
                                                                className="field-error">{meta.error}</div>)}
                                                            <input
                                                                {...input}
                                                                type={(displayPassword && 'text') || 'password'}
                                                                onChange={(value) => {
                                                                    setError(null);
                                                                    input.onChange(value);
                                                                }}
                                                            />
                                                            <label>Mot de passe *</label>
                                                            {displayPassword ? (<span
                                                                className="clear-password"
                                                                onClick={() => setDisplayPassword(false)}
                                                            >
                                      <i className="fas fa-eye-slash"/>
                                    </span>) : (<span
                                                                className="clear-password"
                                                                onClick={() => setDisplayPassword(true)}
                                                            >
                                      <i className="fas fa-eye"/>
                                    </span>)}
                                                        </div>)}
                                                    </Field>
                                                </div>

                                                <div className="btn-group column">
                                                    {error && (<div className="error-block ">
                                <span className="error-icon">
                                  <i className="fas fa-exclamation"/>
                                </span>
                                                        <div className="error-message">{error}</div>
                                                    </div>)}
                                                    <button type="submit" className="btn btn-primary">
                                                        <span>Se connecter</span>
                                                    </button>

                                                    <span className="create-account">
                              Vous n'avez pas encore de compte ?
                              <NavLink className="btn btn-link" to="/addmembre">
                                Créer un compte
                              </NavLink>
                            </span>
                                                </div>
                                            </form>)}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </Fragment>);
};

Login.propTypes = {
    setConfig: PropTypes.func.isRequired, login: PropTypes.func.isRequired
};
//                   <FontAwesomeIcon className="icon" icon={"user"}/>
//                    <FontAwesomeIcon className="icon" icon={"lock"}/>
export default connect(null, {login, setConfig})(Login);






import React from 'react';
import {Link} from "react-router-dom";


function PageNotFound() {
    return (
        <div>
            <h4>Tu t'es perdu ?</h4>
                <img alt=""
                     src="https://media4.giphy.com/media/Js7cqIkpxFy0bILFFA/giphy.gif?cid=ecf05e47937l9crovid2mlovjpvj88qxsfp6q7halshzz8gp&rid=giphy.gif&ct=g"/>
            <p>Cliques ici pour rentrer à la maison avec moi :</p>
            <Link to="/addjeu" > Retour </Link>
        </div>
    );
};

export default PageNotFound;
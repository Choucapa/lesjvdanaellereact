import React, {Fragment} from 'react';
import "./add-jeu.css"
import {ValidatorSchema} from "./add-jeu-validators";
import {ErrorMessage, Field, Form, Formik} from "formik";

import {useNavigate} from "react-router-dom";
import {connect} from "react-redux";


const AddJeu = () => {

    const navigate = useNavigate();
    const initialValues = {
        nomJeu: '',
        jaquette: '',
        support: '',
        online: '',
        description: '',
        noteMoyenne: '',
        commentaire: '',
        nbrDeMembre: ''
    };

    const handleSubmit = (values) => {
        navigate('/listjeu');

    };

    return (
        <Fragment>
            <h3 className="text-center">Ajout d'un jeu</h3>
            <div className=" bloc3 offset-2 col-8">
                <Formik
                    initialValues={initialValues}
                    validationSchema={ValidatorSchema}
                    onSubmit={(values) => handleSubmit(values)}
                >
                    {({resetForm}) => (
                        <Form className="formulaire">

                            <fieldset className="fieldset">
                                <div>
                                    <label htmlFor="nomJeu">Nom du jeu :</label>
                                    <Field type="text"
                                           id="nomJeu" name="nomJeu"
                                           className="form-control"/>
                                    <ErrorMessage name="nomJeu"
                                                  className="text-danger"
                                                  component="small"/>
                                </div>
                                <div>
                                    <label htmlFor="support">Sur quelle console ? : </label>
                                    <Field as="select" name="support" id="support"
                                           className="form-control">
                                        <option> ---</option>
                                        <option value="PS4">PS4</option>
                                        <option value="Switch">Switch</option>
                                        <option value="GameCube">GameCube</option>
                                        <option value="Wii">Wii</option>
                                        <option value="PS2">PS2</option>
                                        <option value="PS1">PS1</option>
                                        <option value="DreamCast">DreamCast</option>
                                        <option value="Xbox">Xbox</option>
                                        <option value="Saturn">Saturn</option>
                                        <option value="GameBoy">GameBoy</option>
                                        <option value="GameBoyAdvance">GameBoyAdvance</option>
                                        <option value="NintendoDS">Nintendo DS</option>
                                        <option value="Nintendo3DS">Nintendo 3ds</option>
                                    </Field>
                                    <ErrorMessage name="support"
                                                  className="text-danger"
                                                  component="small"/>
                                </div>
                                <div>
                                    <label htmlFor="online">
                                        Jeu en ligne disponible :
                                        <br/>
                                        <Field name="online" id="onlineOui" type="radio"
                                               value="true"/>
                                        <label> Oui </label>
                                        <Field name="online" id="onlineNon" type="radio" value="false"
                                        />
                                        <label> Non </label>
                                    </label>
                                    <ErrorMessage name="online"
                                                  className="text-danger"
                                                  component="small"/>
                                </div>
                                <div>
                                    <label htmlFor="description">Description du jeu : </label>
                                    <Field as="textarea"
                                           id="description" name="description"
                                           className="form-control"/>
                                    <ErrorMessage name="description"
                                                  className="text-danger"
                                                  component="small"/>
                                </div>
                            </fieldset>
                            <button type="submit" className="btn btn-outline-primary">
                                Créer
                            </button>
                            <button
                                type="button"
                                onClick={resetForm}
                                className="btn btn-danger">
                                Remise à zéro
                            </button>
                        </Form>)}
                </Formik>
            </div>
        </Fragment>
    );
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {})(AddJeu);

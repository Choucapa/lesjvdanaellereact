import * as Yup from 'yup'

export const ValidatorSchema = Yup.object().shape(
{
    nomJeu: Yup.string()
        .min(3,"Minimum 3 lettres")
        .max(20,"Maximum 20 lettres")
        .required( "Nom requis"),
    support: Yup.string()
        .required ("Support requis"),
    online: Yup.boolean()
        .required ("Champs requis"),
    description: Yup.string()
        .max(500,"Maximum 500 caractères")
}
);
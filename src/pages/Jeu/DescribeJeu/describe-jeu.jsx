import React, {useEffect} from "react";
import {connect} from 'react-redux';
import "./describe-jeu.css"
import AddCommentaire from "../../Commentaire/AddCommentaire/add-commentaire";
import axios from "axios";
import BlocCommentaire from "../../Commentaire/BlocCommentaire/bloc-commentaire";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const DescribeJeu = () => {

    const [jeu, setJeu] = React.useState(null);
    const url_decoupe = document.location.href.split("/");
    const id_jeu = url_decoupe[url_decoupe.length - 1];
    useEffect(() => {
        axios.get(`http://localhost:3000/jeux/${id_jeu}`).then((response) => {
            setJeu(response.data);
        })
    }, []);

    return (
        <div className=" container-fluid">
            <div className=" row col-8 offset-2">
                <div className=" bloc ">
                    <p>
                        Nom du jeu : {jeu && jeu.nomJeu}
                    </p>
                    <p>
                        Sur quelle console : {jeu && jeu.support}
                    </p>
                    <p>
                        Jeu en ligne disponible : {jeu && jeu.online === "true" &&
                        <FontAwesomeIcon className="text-success" icon="check"/>}
                        {jeu && jeu.online === "false" && <FontAwesomeIcon className="text-danger" icon="xmark"/>}
                    </p>
                    <p>
                        Description du jeu : {jeu && jeu.description}
                    </p>
                </div>

            </div>
            <AddCommentaire/>
            <BlocCommentaire/>
        </div>
    )
}

DescribeJeu.propTypes = {};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {})(DescribeJeu);



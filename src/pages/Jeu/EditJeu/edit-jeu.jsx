import React, {Fragment, useEffect, useState} from 'react';
import "../AddJeu/add-jeu.css"

import {useNavigate} from "react-router-dom";
import {connect} from "react-redux";
import axios from "axios";


const EditJeu = () => {

    const [jeu, setJeu] = React.useState(null);
    const [nomJeu, setNomjeu] = useState('');
    const [support, setSupport] = useState('');
    const [online, setOnline] = useState('');
    const [description, setDescription] = useState('');
    const url_decoupe = document.location.href.split("/");
    const id_jeu = url_decoupe[url_decoupe.length - 1];

    useEffect(() => {
        axios.get(`http://localhost:3000/jeux/${id_jeu}`).then((response) => {
            setJeu(response.data);
        })
    }, []);

    const navigate = useNavigate();

    const initialValues = {
        nomJeu: jeu && jeu.nomJeu || "",
        support: jeu && jeu.support || "",
        online: jeu && jeu.online || "",
        description: jeu && jeu.description || ""
    };

    const handleSubmit = () => {
        console.log("test", jeu.id)
        axios
            .put(`localhost:3000/jeux/${jeu.id}`, {
                    nomJeu: nomJeu,
                    support: support,
                    online: online,
                    description: description
                }
            )
            .then(() => navigate('/listjeu'));
    }


    const handleChange = (event) => {
        if (event.target.id === "nomJeu") {
            setNomjeu(event.target.value);
        }
        if (event.target.id === "support") {
            setSupport(event.target.value);
        }
        if (event.target.id === "online") {
            setOnline(event.target.value);
        }
        if (event.target.id === "description") {
            setDescription(event.target.value);
        }

    }


    return (
        <Fragment>
            <h3 className="text-center">Modification de {initialValues.nomJeu}</h3>
            <div className=" bloc3 offset-2 col-8">
                <form className="formulaire">
                    <div>
                        <label htmlFor="nomJeu">Nom du jeu :</label>
                        <input type="text"
                               id="nomJeu" name="nomJeu"
                               className="form-control" defaultValue={initialValues.nomJeu} onChange={handleChange}/>
                    </div>
                    <div>
                        <label htmlFor="support">Sur quelle console ? : </label>
                        <select name="support" id="support"
                                className="form-control" defaultValue={initialValues.support} onChange={handleChange}>
                            <option> ---</option>
                            <option value="PS4">PS4</option>
                            <option value="Switch">Switch</option>
                            <option value="GameCube">GameCube</option>
                            <option value="Wii">Wii</option>
                            <option value="PS2">PS2</option>
                            <option value="PS1">PS1</option>
                            <option value="DreamCast">DreamCast</option>
                            <option value="Xbox">Xbox</option>
                            <option value="Saturn">Saturn</option>
                            <option value="GameBoy">GameBoy</option>
                            <option value="GameBoyAdvance">GameBoyAdvance</option>
                            <option value="NintendoDS">Nintendo DS</option>
                            <option value="Nintendo3DS">Nintendo 3ds</option>
                        </select>
                    </div>
                    <div>
                        <label htmlFor="online">
                            Jeu en ligne disponible :
                            <br/>
                            <input type="checkbox" name="online" id="onlineOui" type="radio"
                                   value="true" onChange={handleChange}/>
                            <label> Oui </label>
                            <input type="checkbox" name="online" id="onlineNon" type="radio" value="false"
                                   onChange={handleChange}/>
                            <label> Non </label>
                        </label>
                    </div>
                    <div>
                        <label htmlFor="description">Description du jeu : </label>
                        <input type="textarea"
                               id="description" name="description"
                               className="form-control" defaultValue={initialValues.description}
                               onChange={handleChange}/>
                    </div>
                    <button type="submit" className="btn btn-outline-primary">
                        Modifier
                    </button>
                </form>
            </div>
        </Fragment>
    )
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {})(EditJeu);

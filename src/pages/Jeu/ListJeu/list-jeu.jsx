import React, {useEffect} from "react";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import "./list-jeu.css"
import {connect} from "react-redux";
import axios from "axios";


const ListJeu = () => {

    const [jeux, setJeux] = React.useState(null);

    useEffect(() => {
        axios.get("http://localhost:3000/jeux").then((response) => {
            setJeux(response.data);
        })
    }, []);

    let listeJeu = {};

    if (jeux && jeux.length) {
        listeJeu = jeux.map(
            (jeu) => (
                <tr key={jeu.id}>
                    <td><Link to={`../desc/${jeu.id}?`}> {jeu.nomJeu}</Link></td>
                    <td>
                        {jeu.support}
                        <img className="img-fluid taille-logo" alt="logo"
                             src={`/images/consoles/${jeu.support}.png`}
                             onError={({currentTarget}) => {
                                 currentTarget.onerror = null; // prevents looping
                                 currentTarget.src = '/images/consoles/null.png'
                             }}
                        />
                    </td>
                    <td>
                        {jeu.online === "true" && <FontAwesomeIcon className="text-success" icon="check"/>}
                        {jeu.online === "false" && <FontAwesomeIcon className="text-danger" icon="xmark"/>}
                    </td>
                    <td>
                        <Link to={`/edit/${jeu.id}`}>
                            <FontAwesomeIcon icon="edit"/>
                        </Link>
                        <FontAwesomeIcon icon="trash"/>
                    </td>
                </tr>
            ))
    }

    return (
        <div className="container">
            <h3 className="text-center"> Liste des jeux-vidéo</h3>
            <table className="table table-hover">
                <thead>
                <tr>
                    <th>Nom du jeu</th>
                    <th>Support</th>
                    <th>Online</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {jeux && jeux.length && listeJeu}
                </tbody>
            </table>
        </div>
    )


}


const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {})(ListJeu)

import React from 'react';
import "./header.css";
import {Link} from "react-router-dom";
import logo from "../../../assets/images/logoljva.jpg";


//https://medium.com/@mattywilliams/generating-an-automatic-breadcrumb-in-react-router-fed01af1fc3
function Header() {
    return (
        <div className="headerClass container-fluid">
            <div className="row">
                <div className="col-4">
                    <img src={logo} aria-label="logo du site"/>
                </div>
                <div className="col-auto">
                    <div className="row">
                        <h3 className="text-center">Les jeux-vidéo d'Anaëlle</h3>
                    </div>
                    <div>
                        <Link className="lien" to="/">Accueil</Link>
                        <Link className="lien" to="/listjeu">Liste des jeux</Link>
                        <Link className="lien" to="/addjeu">Ajout de jeu</Link>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Header;
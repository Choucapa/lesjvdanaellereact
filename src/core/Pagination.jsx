import React from 'react';

const Pagination = ({ jeuParPage, totalJeux, paginate }) => {
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(totalJeux / jeuParPage); i++) {
        pageNumbers.push(i);
    }
// https://github.com/AdeleD/react-paginate avec les ...
    // https://github.com/AdeleD/react-paginate avec les ... et next previous
    return (
        <nav>
            <ul className='pagination justify-content-center'>
                <li className="page-item">
                    <a className="page-link" href="src/core/Pagination#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span className="sr-only">Previous</span>
                    </a>
                </li>
                {pageNumbers.map(number => (
                    <li key={number} className='page-item'>
                        <a onClick={() => paginate(number)}  className='page-link'>
                            {number}
                        </a>
                    </li>
                ))}
                <li className="page-item">
                    <a className="page-link" href="src/core/Pagination#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span className="sr-only">Next</span>
                    </a>
                </li>
            </ul>
        </nav>
    );
};

export default Pagination;